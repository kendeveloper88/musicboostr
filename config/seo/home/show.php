<?php

return [
    [
        'property' => 'og:url',
        'content' =>  '{{URL.BASE}}',
    ],
    [
        'property' => 'og:title',
        'content' => 'Listen to music for free.',
    ],
    [
        'property' => 'og:description',
        'content' => 'Find and listen to millions of songs, albums and artists, all completely free.',
    ],
    [
        'property' => 'keywords',
        'content' => 'music, online, listen, streaming, play, digital, album, artist, playlist'
    ],
    [
        'property' => 'og:type',
        'content' => 'website',
    ],
];